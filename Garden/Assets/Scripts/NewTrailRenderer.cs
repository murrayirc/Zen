﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NewTrailRenderer : MonoBehaviour 
{
    public bool emit = true;
    [SerializeField] float emitTime = 0.00F;
    [SerializeField] Material material;

    [SerializeField] float lifeTime = 1.00F;

    [SerializeField] Color[] colors;
    [SerializeField] float[] sizes;

    [SerializeField] float uvLengthScale = 0.01F;
    [SerializeField] bool higherQualityUVs = true;

    [SerializeField] int movePixelsForRebuild = 6;
    [SerializeField] float maxRebuildTime = 0.1F;

    [SerializeField] float minVertexDistance = 0.10F;

    [SerializeField] float maxVertexDistance = 10.00F;
    [SerializeField] float maxAngle = 3.00F;

    [SerializeField] bool autoDestruct = false;

    List<Point> points = new List<Point>();
    GameObject o;
    Vector3 lastPosition;
    Vector3 lastCameraPosition1;
    Vector3 lastCameraPosition2;
    float lastRebuildTime = 0.00F;
    bool lastFrameEmit = true;

    public class Point
    {
        public float timeCreated = 0.00F;
        public Vector3 position;
        public bool lineBreak = false;
    }

    void Start()
    {
        lastPosition = transform.position;
        o = new GameObject("Trail");
        o.transform.parent = null; //
        o.transform.position = Vector3.zero;
        o.transform.rotation = Quaternion.identity;
        o.transform.localScale = Vector3.one;
        o.AddComponent<MeshFilter>(); //
        o.AddComponent<MeshRenderer>(); //
        o.GetComponent<MeshRenderer>().material = material; //
    }

    void OnEnable()
    {
        lastPosition = transform.position;
        o = new GameObject("Trail");
        o.transform.parent = null; //
        o.transform.position = Vector3.zero;
        o.transform.rotation = Quaternion.identity;
        o.transform.localScale = Vector3.one;
        o.AddComponent<MeshFilter>(); //
        o.AddComponent<MeshRenderer>(); //
        o.GetComponent<MeshRenderer>().material = material; //
    }

    void OnDisable()
    {
        Destroy(o);
    }

    void Update()
    {
        // GTFO if there is no camera
        if (!Camera.main)
        {
            return;
        }

        ControlTimedEmission();
        AutoDestruct();
        Emission();
    }

    void ControlTimedEmission()
    {
        // Control Timed Emission
        if (emit && emitTime != 0)
        {
            emitTime -= Time.deltaTime;
            if (emitTime == 0)
            {
                emitTime = -1;
            }
            if (emitTime < 0)
            {
                emit = false;
            }
        }
    }

    void AutoDestruct()
    {
        // AutoDestruct
        if (!emit && points.Count == 0 && autoDestruct)
        {
            Destroy(o);
            Destroy(gameObject);
        }
    }

    void Emission()
    {
        // Emission
        bool re = false;
        float theDistance = (lastPosition - transform.position).magnitude;
        if (emit)
        {
            if (theDistance > minVertexDistance)
            {
                bool make = false;
                if (points.Count < 3)
                {
                    make = true;
                }
                else
                {
                    Vector3 L1 = points[points.Count - 2].position - points[points.Count - 3].position;
                    Vector3 L2 = points[points.Count - 1].position - points[points.Count - 2].position;
                    if (Vector3.Angle(L1, L2) > maxAngle || theDistance > maxVertexDistance)
                    {
                        make = true;
                    }
                }

                if (make)
                {
                    Point p = new Point();
                    p.position = transform.position;
                    p.timeCreated = Time.time;
                    points.Add(p);
                    lastPosition = transform.position;
                }
                else
                {
                    points[points.Count - 1].position = transform.position;
                    points[points.Count - 1].timeCreated = Time.time;
                }
            }
            else if (points.Count > 0)
            {
                points[points.Count - 1].position = transform.position;
                points[points.Count - 1].timeCreated = Time.time;
            }
        }

        if (!emit && lastFrameEmit && points.Count > 0)
        {
            points[points.Count - 1].lineBreak = true;
        }
        lastFrameEmit = emit;

        // Calculate whether or not the mesh should be rebuilt.
        if (points.Count > 1)
        {
            Vector3 cur1 = Camera.main.WorldToScreenPoint(points[0].position);
            lastCameraPosition1.z = 0F;
            Vector3 cur2 = Camera.main.WorldToScreenPoint(points[points.Count - 1].position);
            lastCameraPosition2.z = 0F;

            float distance = (lastCameraPosition1 - cur1).magnitude;
            distance += (lastCameraPosition2 - cur2).magnitude;

            if (distance > movePixelsForRebuild || Time.time - lastRebuildTime > maxRebuildTime)
            {
                re = true;
                lastCameraPosition1 = cur1;
                lastCameraPosition2 = cur2;
            }
        }
        else
        {
            re = true;
        }

        if (re)
        {
            lastRebuildTime = Time.time;

            int i = 0;
            foreach (Point p in points)
            {
                // Cull old points first.
                if (Time.time - p.timeCreated > lifeTime)
                {
                    points.RemoveAt(i);
                }
                i++;
            }

            if (points.Count > 1)
            {
                Vector3[] newVertices = new Vector3[points.Count * 2];
                Vector2[] newUV = new Vector2[points.Count * 2];
                int[] newTriangles = new int[(points.Count - 1) * 6];
                Color[] newColors = new Color[points.Count * 2];

                i = 0;
                float curDistance = 0.00F;

                foreach (Point p in points)
                {
                    float time = (Time.time - p.timeCreated) / lifeTime;
                    Color color;
                    float size;

                    if (colors.Length > 0) // this might be wonky
                    {
                        float colorTime = time * (colors.Length - 1);
                        float min = Mathf.Floor(colorTime);
                        float max = Mathf.Clamp(Mathf.Ceil(colorTime), 1F, colors.Length - 1);
                        float lerp = Mathf.InverseLerp(min, max, colorTime);
                        //if (min >= colors.Length) min = colors.Length - 1; 
                        //if (min < 0) min = 0;
                        //if (max >= colors.Length) max = colors.Length - 1; 
                        //if (max < 0) max = 0;
                        color = Color.Lerp(colors[(int)min], colors[(int)max], lerp);
                    }
                    else
                    {
                        color = Color.Lerp(Color.white, Color.clear, time);
                    }

                    if (sizes.Length > 0) // same here
                    {
                        float sizeTime = time * (sizes.Length - 1);
                        float min = Mathf.Floor(sizeTime);
                        float max = Mathf.Clamp(Mathf.Ceil(sizeTime), 1F, sizes.Length - 1);
                        float lerp = Mathf.InverseLerp(min, max, sizeTime);
                        if (min >= sizes.Length) min = sizes.Length - 1; 
                        if (min < 0) min = 0;
                        if (max >= sizes.Length) max = sizes.Length - 1; 
                        if (max < 0) max = 0;
                        size = Mathf.Lerp(sizes[(int)min], sizes[(int)max], lerp);
                    }
                    else
                    {
                        size = 1;
                    }

                    Vector3 lineDirection = Vector3.zero;
                    if (i == 0)
                    {
                        lineDirection = p.position - points[i + 1].position;
                    }
                    else
                    {
                        lineDirection = points[i - 1].position - p.position;
                    }

                    Vector3 vectorToCamera = Camera.main.transform.position - p.position;

                    Vector3 perpendicular = Vector3.Cross(lineDirection, vectorToCamera).normalized;

                    newVertices[i * 2] = p.position + (perpendicular * (size * 0.5F));
                    newVertices[(i * 2) + 1] = p.position + (-perpendicular * (size * 0.5F));

                    newColors[i * 2] = newColors[(i * 2) + 1] = color;

                    newUV[i * 2] = new Vector2(curDistance * uvLengthScale, 0F);
                    newUV[(i * 2) + 1] = new Vector2(curDistance * uvLengthScale, 1F);

                    if (i > 0 && !points[i - 1].lineBreak)
                    {
                        if (higherQualityUVs)
                        {
                            curDistance += (p.position - points[i - 1].position).magnitude;
                        }
                        else
                        {
                            curDistance += (p.position - points[i - 1].position).sqrMagnitude;
                        }

                        newTriangles[(i - 1) * 6] = (i * 2) - 2;
                        newTriangles[((i - 1) * 6) + 1] = (i * 2) - 1;
                        newTriangles[((i - 1) * 6) + 2] = i * 2;

                        newTriangles[((i - 1) * 6) + 3] = (i * 2) + 1;
                        newTriangles[((i - 1) * 6) + 4] = i * 2;
                        newTriangles[((i - 1) * 6) + 5] = (i * 2) - 1;
                    }

                    i++;
                }

                Mesh mesh = o.GetComponent<MeshFilter>().mesh;
                mesh.Clear();
                mesh.vertices = newVertices;
                mesh.colors = newColors;
                mesh.uv = newUV;
                mesh.triangles = newTriangles;
            }
        }
    }
}
