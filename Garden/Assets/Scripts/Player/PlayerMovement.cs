﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour 
{
    public float moveSpeed = 1500F;
    public float rotateSpeed = 1000F;

    Rigidbody rb;

    public static bool frozen;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        frozen = true;
        Invoke("waitToUnFreeze", 2F);
    }

    void Update()
    {
        Move();
        Turn();
    }

    void Move()
    {
        // grab horizontal and vertical input values
        float horizontal = Input.GetAxis("Alternate Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(horizontal, 0f, vertical);

        // add force to the rigidbody based on the movement input
        if (!frozen)
        {
            rb.AddRelativeForce(movement * moveSpeed * Time.deltaTime);
        }    
    }

    void Turn()
    {
        float rotate = Input.GetAxis("Horizontal");
        Vector3 rotation = new Vector3(0F, rotate, 0F);
        if (!frozen)
        {
            rb.AddTorque(rotation * rotateSpeed * Time.deltaTime);
        }
    }

    void waitToUnFreeze()
    {
        frozen = false;
    }
}