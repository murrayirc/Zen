﻿using UnityEngine;
using System.Collections;

public class GrabObject : MonoBehaviour 
{
    [SerializeField] private float grabLength = 1.5F;

    AudioSource grabSound;
    AudioSource throwSound;

    void Start()
    {
        grabSound = GetComponents<AudioSource>()[0];
        throwSound = GetComponents<AudioSource>()[1];
    }

    void Update()
    {
        Grab();
    }

    void Grab()
    {
        Debug.DrawRay(new Vector3(transform.position.x, 0.2F, transform.position.z), transform.forward * grabLength, Color.red);

        if (Input.GetMouseButtonDown(0))
        {
            // lean forward and grab animation
            
            // create rays between player and forward vector
            Ray grabRay = new Ray(new Vector3(transform.position.x, .2F, transform.position.z), transform.forward);
            RaycastHit grabHit;
            
            if (transform.childCount == 0)
            {
                if (Physics.Raycast(grabRay, out grabHit, grabLength))
                {
                    if (grabHit.collider)
                    {
                        foreach (GameObject o in ObjectManager.startingObjects)
                        {
                            if (grabHit.collider == o.GetComponent<Collider>())
                            { 
                                if (o.GetComponent<Bucket>())
                                {
                                    Debug.Log("Grabbing Bucket");
                                    o.GetComponent<Collider>().isTrigger = true;
                                }
                                else
                                {
                                    Debug.Log("Grabbing Rake");
                                }
                                o.GetComponent<GrabbableObject>().grabbed = true;
                                o.GetComponent<Rigidbody>().isKinematic = true;
                                o.transform.parent = transform;
                                grabSound.Play();
                            }
                        }

                        if (grabHit.collider == ObjectManager.c.GetComponent<Collider>())
                        {
                            if (!CameraManager.cushChange)
                            {
                                PlayerMovement.frozen = true;
                                CameraManager.cushChange = true;
                                transform.parent = ObjectManager.c.transform;
                                GetComponent<Rigidbody>().velocity = Vector3.zero;
                                transform.localPosition = new Vector3(0F, transform.position.y, 0F);
                                transform.localRotation = Quaternion.identity;
                            }
                        }

                        if (grabHit.collider == FindObjectOfType<Radio>().GetComponent<Collider>())
                        {
                            if (!FindObjectOfType<Radio>().radioSound.isPlaying)
                            {
                                FindObjectOfType<Radio>().radioSound.Play();
                            }
                            else if (FindObjectOfType<Radio>().radioSound.isPlaying)
                            {
                                FindObjectOfType<Radio>().radioSound.Stop();
                            }
                        }
                        
                        if (ObjectManager.seeds.Count != 0)
                        {
                            foreach (GameObject s in ObjectManager.seeds)
                            {
                                if (grabHit.collider == s.GetComponent<Collider>())
                                {
                                    Debug.Log("Grabbing Seed");
                                    s.GetComponent<Rigidbody>().isKinematic = true;
                                    s.GetComponentInChildren<Collider>().enabled = false;
                                    s.GetComponent<GrabbableObject>().grabbed = true;
                                    s.GetComponent<Seed>().Invoke("togglePlantable", 1F);
                                    s.transform.parent = transform;
                                    grabSound.Play();
                                }
                            }
                        }
                    }
                }
            }
        }

        // throw object
        if (Input.GetMouseButton(1))
        {
            if(transform.childCount != 0)
            {
                throwSound.Play();

                if (GetComponentInChildren<Bucket>())
                {
                    GameObject bucket = GetComponentInChildren<Bucket>().gameObject;
                    bucket.GetComponent<Rigidbody>().isKinematic = false;
                    bucket.GetComponent<Collider>().isTrigger = false;
                    bucket.GetComponent<GrabbableObject>().grabbed = false;
                    bucket.GetComponent<Rigidbody>().AddForce(transform.forward * 500F);
                    transform.DetachChildren();
                    
                }
                else if (GetComponentInChildren<Rake>())
                {
                    GameObject rake = GetComponentInChildren<Rake>().gameObject;
                    rake.GetComponent<Rigidbody>().isKinematic = false;
                    rake.GetComponent<GrabbableObject>().grabbed = false;
                    rake.GetComponent<Rigidbody>().AddForce(transform.forward * 500F);
                    transform.DetachChildren();
                }
                else if (GetComponentInChildren<Seed>())
                {
                    GameObject seed = GetComponentInChildren<Seed>().gameObject;
                    seed.GetComponent<Rigidbody>().isKinematic = false;
                    seed.GetComponentInChildren<Collider>().enabled = true;
                    seed.GetComponent<GrabbableObject>().grabbed = false;
                    seed.GetComponent<Seed>().togglePlantable();
                    seed.GetComponent<Rigidbody>().AddForce(transform.forward * 500F);
                    transform.DetachChildren();
                }
            }
        }
    }
}