﻿using UnityEngine;
using System.Collections;

public class FallenObject : MonoBehaviour 
{
    // set this in the inspector
    public bool grounded;
    public ParticleSystem fall;
    public ParticleSystem groundHit;
    public AudioSource fallSound;
    public AudioSource groundHitSound;
    public GameObject sand;

    bool sink = false;

    void Awake()
    {
        fallSound = GetComponents<AudioSource>()[0];
        groundHitSound = GetComponents<AudioSource>()[1];
        sand = GameObject.FindGameObjectWithTag("Sand");
    }

    void Start()
    {
        fall.Play();
        fallSound.Play();
        grounded = false;
        Invoke("StartSink", 500F);
    }

    void Update()
    {
        Sink();
    }

    void Sink()
    {
        if (sink)
        {
            if (transform.position.y <= -5F)
            {
                Destroy(this.gameObject);
            }
            else
            {
                transform.Translate(Vector3.down * .1F * Time.deltaTime);
            }
        }
    }

    void StartSink()
    {
        sink = true;
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.collider && !grounded)
        {
            fall.Stop();
            fallSound.Stop();
            groundHitSound.Play();
            groundHit.Play();
            StartCoroutine("destroyParticles");
            GetComponent<Rigidbody>().isKinematic = true; // remove this if you want to allow the player to move fallen objects
            grounded = true;
        }
    }

    IEnumerator destroyParticles()
    {
        yield return new WaitForSeconds(2F);
        Destroy(fall.gameObject);
        Destroy(groundHit.gameObject);
        yield return null;
    }
}