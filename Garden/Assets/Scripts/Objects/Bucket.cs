﻿using UnityEngine;
using System.Collections;

public class Bucket : MonoBehaviour 
{
    public bool hasWater;
    public bool touchingWater;

    public MeshRenderer emptyBucket;
    public MeshRenderer filledBucket;
    public GameObject water;
    public AudioSource fillSound;
    public AudioSource growSound;
    public AudioSource sandSound;

    void Start()
    {
        emptyBucket = GetComponentsInChildren<MeshRenderer>()[0];
        filledBucket = GetComponentsInChildren<MeshRenderer>()[1];
        fillSound = GetComponents<AudioSource>()[0];
        growSound = GetComponents<AudioSource>()[1];
        sandSound = GetComponents<AudioSource>()[2];
    }

    void Update()
    {
        FillBucket();
    }

    void FillBucket()
    {
        if (Input.GetMouseButton(0) && touchingWater && GetComponent<GrabbableObject>().grabbed && !hasWater)
        {
            hasWater = true;
            emptyBucket.enabled = false;
            filledBucket.enabled = true;
            fillSound.Play();
        }   
    }

    void OnTriggerEnter(Collider other)
    {
        if (other == water.GetComponent<Collider>())
        {
            touchingWater = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other == water.GetComponent<Collider>())
        {
            touchingWater = false;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.collider == GameObject.FindGameObjectWithTag("Sand").GetComponent<Collider>())
        {
            sandSound.Play();
        }
    }
}