﻿using UnityEngine;
using System.Collections;

public class Seed : MonoBehaviour 
{
    public GameObject[] plantArray = new GameObject[3];

    public GameObject plant;
    bool canPlant = false;

    AudioSource sandSound;


    void Start()
    {
        int randPlant = Random.Range(0, plantArray.Length);
        plant = plantArray[randPlant];

        sandSound = GetComponent<AudioSource>();
    }

    void Update()
    {
        PlantSeed();
    }

    void PlantSeed()
    {
        if (Input.GetMouseButton(0) && GetComponent<GrabbableObject>().grabbed && canPlant)
        {
            Instantiate(plant, new Vector3(transform.position.x, 0F, transform.position.z), Quaternion.identity);
            ObjectManager.seeds.Remove(this.gameObject);
            Destroy(this.gameObject);
        }
    }

    public void togglePlantable()
    {
        canPlant = !canPlant;
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.collider == GameObject.FindGameObjectWithTag("Sand").GetComponent<Collider>())
        {
            sandSound.Play();
        }
    }
}
