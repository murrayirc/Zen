﻿using UnityEngine;
using System.Collections;

public class Plant : MonoBehaviour 
{
    // this is what we'll be using to alter the scale of the plant
    public Transform plantModel;

    // set in inspector
    public GameObject seed;

    public bool touchingWater;

    public int plantLevel;
    public int prevLevel;

    bool grow = false;
    [SerializeField] float growSpeed = .1F;

    ParticleSystem readyToGrow;

    BoxCollider plantCollider;

    void Start()
    {
        plantModel = transform.GetChild(0);
        plantLevel = 0;
        prevLevel = 0;
        readyToGrow = GetComponentInChildren<ParticleSystem>();
        readyToGrow.Play();
        plantCollider = GetComponent<BoxCollider>();
    }

    void Update()
    {
        WaterPlant();
        GrowCheck();
    }

    void WaterPlant()
    {
        Bucket bucket = ObjectManager.startingObjects[0].GetComponent<Bucket>();
        if (Input.GetMouseButton(0) && bucket.hasWater && bucket.GetComponent<GrabbableObject>().grabbed && touchingWater && !grow)
        {
            plantLevel++;
            bucket.hasWater = false;
            bucket.emptyBucket.enabled = true;
            bucket.filledBucket.enabled = false;
            bucket.growSound.Play();
            readyToGrow.Stop();
        }
    }

    void GrowCheck()
    {
        if (plantLevel != prevLevel)
        {
            grow = true;
            prevLevel = plantLevel;
        }

        if (grow)
        {
            Grow();
        }
    }

    void Grow()
    {
        Vector3 newScale = transform.localScale;

        if (plantLevel == 1)
        {
            newScale = new Vector3(1.5F, 1.5F, 1.5F);
        }
        else if (plantLevel == 2)
        {
            newScale = new Vector3(2F, 2F, 2F);
        }
        else if (plantLevel == 3)
        {
            newScale = new Vector3(2.5F, 2.5F, 2.5F);
        }
        else if (plantLevel == 4)
        {
            newScale = new Vector3(3F, 3F, 3F);
        }

        
        if (transform.localScale.x >= newScale.x)
        {
            grow = false;

            if (plantLevel < 4)
            {
                readyToGrow.Play();
            }

            if (plantLevel == 4)
            {
                Destroy(plantCollider);
                StartCoroutine(spawnSeeds());
            }
        }
        else
        {
            transform.localScale = Vector3.MoveTowards(transform.localScale, newScale, growSpeed * Time.deltaTime);
        }
    }

    IEnumerator spawnSeeds()
    {
        GameObject s1 = (GameObject)Instantiate(seed, new Vector3(transform.position.x - 1F, 2F, transform.position.z), Quaternion.identity);
        ObjectManager.seeds.Add(s1);
        GameObject s2 = (GameObject)Instantiate(seed, new Vector3(transform.position.x + 1F, 2F, transform.position.z), Quaternion.identity);
        ObjectManager.seeds.Add(s2);

        yield return null;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == ObjectManager.startingObjects[0])
        {
            touchingWater = true;
        }

        else
        {
            foreach(GameObject o in ObjectManager.fallenObjectList)
            {
                if (other.gameObject == o)
                {
                    Destroy(this.gameObject);
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == ObjectManager.startingObjects[0])
        {
            touchingWater = false;
        }
    }
}
