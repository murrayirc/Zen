﻿using UnityEngine;
using System.Collections;

public class Rake : MonoBehaviour 
{
    public bool emit;
    AudioSource rakeSound;
    AudioSource sandSound;

    void Start()
    {
        rakeSound = GetComponents<AudioSource>()[0];
        sandSound = GetComponents<AudioSource>()[1];
    }

    void Update()
    {
        EmitTrail();
    }

    void EmitTrail()
    {
        if (emit)
        {
            foreach (NewTrailRenderer t in GetComponentsInChildren<NewTrailRenderer>())
            {
                t.emit = true;
            }

            if (!rakeSound.isPlaying)
            {
                rakeSound.Play();
            }  
        }
        else
        {
            foreach (NewTrailRenderer t in GetComponentsInChildren<NewTrailRenderer>())
            {
                t.emit = false;
            }

            if (rakeSound.isPlaying)
            {
                rakeSound.Stop();
            }
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.collider)
        {
            if (other.collider == GameObject.FindGameObjectWithTag("Sand").GetComponent<Collider>())
            {
                sandSound.Play();
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other && emit)
        {
            foreach (Plant p in GameObject.FindObjectsOfType<Plant>())
            {
                if (other == p.GetComponent<Collider>())
                {
                    Destroy(p.gameObject);
                }
            }
        }
    }
}
