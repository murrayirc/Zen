﻿using UnityEngine;
using System.Collections;

public class Radio : MonoBehaviour 
{
    // set this in the inspector
    public AudioSource radioSound;

    bool sink = false, rise = false;

    void Start()
    {
        radioSound = GetComponent<AudioSource>();
        Invoke("StartRise", 250F);
    }

    void Update()
    {
        Sink();
        Rise();
    }

    void Sink()
    {
        if (sink)
        {
            if (transform.position.y <= -5F)
            {
                Invoke("StartRise", 250F);
                sink = false;
            }
            else
            {
                transform.Translate(Vector3.down * 1F * Time.deltaTime);
            }
        }
    }

    void Rise()
    {
        if (rise)
        {
            if (transform.position.y >= 0F)
            {
                Invoke("StartSink", 250F);
                rise = false;
            }
            else
            {
                transform.Translate(Vector3.up * 1F * Time.deltaTime);
            }
        }
    }

    void StartSink()
    {
        sink = true;
    }

    void StartRise()
    {
        rise = true;
    }
}
