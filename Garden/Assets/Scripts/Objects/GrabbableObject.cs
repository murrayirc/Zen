﻿using UnityEngine;
using System.Collections;

public class GrabbableObject : MonoBehaviour 
{
    public bool grabbed;

    void Start()
    {
        grabbed = false;
    }

    void Update()
    {
        PickUp();
    }

    void PickUp()
    {
        if (grabbed)
        {
            if (GetComponent<Seed>())
            {
                StartCoroutine(Levitate(.5F, 1F));
                transform.Rotate(new Vector3(15F, 30F, 45F) * Time.deltaTime);  
            }
            if (GetComponent<Rake>())
            {
                StartCoroutine(Levitate(.75F, 1.5F));
                if (Input.GetMouseButton(0))
                {
                    transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.Euler(new Vector3(20F, 0F, 0F)), 250F * Time.deltaTime);
                    if (transform.localRotation == Quaternion.Euler(new Vector3(20F, 0F, 0F)))
                    {
                        GetComponent<Rake>().emit = true;
                    } 
                }
                else
                {
                    transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.Euler(new Vector3(320F, 0F, 0F)), 250F * Time.deltaTime);
                    GetComponent<Rake>().emit = false;
                }
                
            }
            if (GetComponent<Bucket>())
            {
                StartCoroutine(Levitate(.75F, 1.5F));
                transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.identity, 250F * Time.deltaTime);
            }
        }
    }

    IEnumerator Levitate(float levHeight, float distanceFromPlayer)
    {
        while (transform.localPosition.y < levHeight && grabbed)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(0F, levHeight, distanceFromPlayer), 10F * Time.deltaTime);
            yield return null;
        }
    }
}
