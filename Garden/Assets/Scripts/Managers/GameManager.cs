﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour 
{
    public Text titleBlack;
    public Text titleWhite;
    public float fadeSpeed = .5F;

    void Start()
    {
        StartCoroutine("fadeTitle");
        Invoke("DisableTitle", 10F);
    }

    void Update()
    {
        Quit();
    }

    IEnumerator fadeTitle()
    {
        while (titleBlack.color.a > Color.clear.a && titleWhite.color.a > Color.clear.a)
        {
            titleBlack.color = Color.Lerp(titleBlack.color, Color.clear, fadeSpeed * Time.deltaTime);
            titleWhite.color = Color.Lerp(titleWhite.color, Color.clear, fadeSpeed * Time.deltaTime);

            yield return null;
        }
    }

    void DisableTitle()
    {
        titleBlack.enabled = false;
        titleWhite.enabled = false;
        StopCoroutine("fadeTitle");
    }

    void Quit()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
