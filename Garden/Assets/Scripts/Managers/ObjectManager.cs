﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectManager : MonoBehaviour 
{
    public List<GameObject> objectList = new List<GameObject>();
    public static List<GameObject> fallenObjectList = new List<GameObject>();

    // bucket = 0, rake = 1, water = 3
    public static GameObject[] startingObjects = new GameObject[3];
    public GameObject bucket;
    public GameObject rake;
    public GameObject water;

    public GameObject cushion;
    public static GameObject c;  // This is the instantiated cushion.

    public GameObject radio;
    public static GameObject r;

    public static List<GameObject> seeds = new List<GameObject>();
    public GameObject seed;

    public GameObject[] plants = new GameObject[3];

    private float fallTimer;
    [SerializeField] private float fallTimeInterval = 75F;

    void Start()
    {
        fallTimer = 0F;
        StartCoroutine("SpawnStartingObjects");
        StartCoroutine("SpawnRandomPlant");
    }

    void Update()
    {
        fallTimer += Time.deltaTime;

        if (fallTimer >= fallTimeInterval && objectList.Count > 0)
        {
            StartCoroutine(SpawnRandomObject());
            fallTimer = 0F;
        }
    }

    IEnumerator SpawnStartingObjects()
    {
        // spawn bucket
        GameObject b = (GameObject)Instantiate(bucket, new Vector3(0F, 1F, -5F), Quaternion.identity);
        startingObjects[0] = b;

        // spawn rake
        GameObject r = (GameObject)Instantiate(rake, new Vector3(0F, 1F, -20F), Quaternion.identity);
        startingObjects[1] = r;

        // spawn water
        GameObject w = (GameObject)Instantiate(water, new Vector3(0F, .4F, 0F), Quaternion.identity);
        b.GetComponent<Bucket>().water = w;
        startingObjects[2] = w;

        // spawn first seed
        GameObject s = (GameObject)Instantiate(seed, new Vector3(0F, 1F, -7.5F), Quaternion.identity);
        seeds.Add(s);

        // spawn cushion
        c = (GameObject)Instantiate(cushion, new Vector3(0F, 0F, -25F), Quaternion.identity);

        // spawn hidden radio
        r = (GameObject)Instantiate(radio, new Vector3(0F, -5F, 25F), Quaternion.identity);

        yield return null;
    }

    IEnumerator SpawnRandomObject()
    {
        // pick a random object from the list to spawn
        int randomIndex = Random.Range(0, objectList.Count);
        GameObject randomObj = (GameObject)Instantiate(objectList[randomIndex], new Vector3(Random.Range(-30F, 30F), transform.position.y + 100F, Random.Range(-30F, 30F)), Quaternion.identity);
        fallenObjectList.Add(randomObj);
        //objectList.Remove(objectList[randomIndex]);

        yield return null;
    }

    IEnumerator SpawnRandomPlant()
    {
        yield return new WaitForSeconds(175);

        int randomIndex = Random.Range(0, plants.Length);
        Instantiate(plants[randomIndex], new Vector3(Random.Range(-30F, 30F), 0F, Random.Range(-30F, 30F)), Quaternion.identity); 

        StartCoroutine("SpawnRandomPlant");
    }
}
