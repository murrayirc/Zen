﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraManager : MonoBehaviour 
{
    // Track current and previous cushions (L0, R1, B2, F3)
    public static bool cushChange;

    // Rotation speed.
    public int rotSpeed;

    // Target Y rotation.
    float targetY;

    // Flags.
    bool rotate = false, clamp = false;

    // Camera Follow
    public Transform target;
    [SerializeField] private float smoothFollow = 5F;

    Vector3 offset;

    public Image fadePanel;

    AudioSource cushStart;
    AudioSource cushEnd;

    void Start()
    {
        cushChange = false;

        fadePanel.enabled = true;
        StartCoroutine("FadeIn");
        StartCoroutine("CheckClamp");

        // calculate offset, which is the vector between the position of the camera and the position of the player
        offset = transform.position - target.position;

        cushStart = GetComponents<AudioSource>()[0];
        cushEnd = GetComponents<AudioSource>()[1];
    }

    void FixedUpdate()
    {
        RotateCamera();
        Follow();
    }

    void RotateCamera()
    {
        // If I'm not already rotating and I sit on a cushion.
        if (!rotate && cushChange)
        {
            targetY = Mathf.Round(transform.eulerAngles.y / 90) * 90 + 90;
            rotate = true;
            cushStart.Play();
        }

        // Rotate if flag for rotation is true.
        if (rotate)
        {
            Rotate();
        }
    }

    void Rotate()
    {
        if (transform.eulerAngles.y >= targetY || clamp)
        {
            // Set rotation flag to false.
            rotate = false;

            // Set clamp flag back to false.
            clamp = false;

            // Allow the player to move again.
            PlayerMovement.frozen = false;

            cushChange = false;

            cushEnd.Play();
        }

        else
        {
            // Rotate camera around player.
            transform.RotateAround(target.position, Vector3.up, rotSpeed * Time.deltaTime);
        }
    }

    void Follow()
    {
        // calculate the position the camera should be lerping towards in world space
        Vector3 targetCamPos = target.position + offset;

        // lerp to the target camera position
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothFollow * Time.deltaTime);
    }

    IEnumerator CheckClamp()
    {
        // Store temporary angle to compare with change after a frame.
        float tmpCam = transform.eulerAngles.y;

        // Wait one frame before executing the following code.
        yield return Time.deltaTime;

        // If the camera's y rotation changes by more than 180 degrees in one frame (this only happens when clamping from 360 to 0)
        if (Mathf.Abs(tmpCam - transform.eulerAngles.y) > 180)
        {
            // Set flag clamp to true.
            clamp = true;
        }

        // Restart coroutine.
        StartCoroutine("CheckClamp");
    }

    IEnumerator FadeOut()
    {
        while (fadePanel.color.a < Color.black.a)
        {
            fadePanel.color = Color.Lerp(fadePanel.color, Color.black, .5F * Time.deltaTime);
            yield return null;
        }
    }

    IEnumerator FadeIn()
    {
        while (fadePanel.color.a > Color.clear.a)
        {
            fadePanel.color = Color.Lerp(fadePanel.color, Color.clear, .35F * Time.deltaTime);
            yield return null;
        }
    }
}
