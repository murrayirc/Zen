﻿using UnityEngine;
using System.Collections;

public class LockMouse : MonoBehaviour
{
    void Update()
    {
        Lock();
    }

    void Lock()
    {
        if (Input.GetMouseButton(0) && Time.timeScale > 0.0f)
        {
            LockCursor(true);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            LockCursor(false);
        }
    }

    public void LockCursor(bool lockCursor)
    {
        if (lockCursor)
            Cursor.lockState = CursorLockMode.Locked;
        else if (!lockCursor)
            Cursor.lockState = CursorLockMode.None;
        Cursor.visible = !lockCursor;
    }
}
